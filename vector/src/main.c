y#include <stdio.h>
#include <stdlib.h>

#include "vector.h"

void print_vector(vector *v){
 vectorErrorCode ret;

	for (int i = 0; i < Vector_End(v,&ret); i++)
        printf("%d ", *((int *) Vector_Get(v, i,&ret)));
    printf("\n");

}

void main(void)
{
	int a =1;
	int b =2;
	int c =3;
	int d =4;

    vector v;
    vector v2;
    vectorErrorCode ret;
    Vector_Init(&v,&ret);
    Vector_Init(&v2,&ret);

    Vector_Add(&v, &a,&ret);
    Vector_Add(&v, &b,&ret);
    Vector_Add(&v, &c,&ret);
    Vector_Add(&v, &d,&ret);

    Vector_Add(&v2, &d,&ret);
    Vector_Add(&v2, &c,&ret);
    Vector_Add(&v2, &b,&ret);
    Vector_Add(&v2, &a,&ret);

    print_vector(&v);
    print_vector(&v2);

    Vector_Swap(&v,&v2,&ret);

    print_vector(&v);
    print_vector(&v2);


    Vector_Delete(&v, 0,&ret);
    print_vector(&v);

    printf("BACK %d\n", *(int *)Vector_Back(&v,&ret));
    Vector_Delete(&v, 3,&ret);
    printf("BACK %d\n", *(int *)Vector_Back(&v,&ret));
    Vector_Delete(&v, 2,&ret);
    printf("BACK %d\n", *(int *)Vector_Back(&v,&ret));
    Vector_Delete(&v, 1,&ret);
    printf("BACK %d\n", *(int *)Vector_Back(&v,&ret));

    Vector_Set(&v, 0, "Hello",&ret);
    Vector_Add(&v, "World",&ret);

    for (int i = 0; i < Vector_End(&v,&ret); i++)
        printf("%s ", (char *) Vector_Get(&v, i,&ret));
    printf("\n");

    Vector_Free(&v,&ret);
}
