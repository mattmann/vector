
#include "common_data_types.h"
#include "common_functions.h"
#include "util_malloc.h"
#include "apex.h"

#include "vector.h"


vectorItem Vector_New_Item(unsigned int size)
{
	vectorItem item;
	RETURN_CODE_TYPE ret;

	ret = UTIL_Malloc(CACHE_DISABLE, &item ,2, size);
	if ( ret != NO_ERROR)
	{
		item = NULL;
	}
	return item;
}

void Vector_Init(vector * const v, unsigned int itemSize,  vectorErrorCode * const ret)
{
	v->capacity = VECTOR_INIT_CAPACITY;
	v->total = 0;
	v->itemSize = itemSize;

	v->items = Vector_New_Item(sizeof(void *) * v->capacity);
	if ( v->items != NULL) {
		*ret = VECTOR_OK;
	} else {
		*ret = VECTOR_ERROR;
	}
}
iterator Vector_Size(vector const * const v, vectorErrorCode * const ret)
{
	return Vector_End(v,ret);
}
iterator Vector_End(vector const * const v, vectorErrorCode * const ret)
{
	*ret = VECTOR_OK;
	return v->total;
}
void Vector_Push_Back(vector * const v, vectorItem item, vectorErrorCode * const ret)
{
	Vector_Add(v,item,ret);
}

void Vector_Add(vector * const v, vectorItem item, vectorErrorCode * const ret)
{
	if (v->capacity == v->total) {
		*ret = VECTOR_FULL;
	} else {
		/*v->items[v->total] = item;*/
		v->items[v->total]= Vector_New_Item(v->itemSize);
		if ( v->items[v->total] != NULL) {
			Memcpy(v->items[v->total],item, v->itemSize);
			v->total++;
			*ret = VECTOR_OK;
		} else {
			*ret = VECTOR_ERROR;
		}
	}
}
void Vector_Insert(vector * const v, iterator idx, vectorItem item, vectorErrorCode * const ret)
{
	iterator i;

	if (v->capacity == v->total) {
		*ret = VECTOR_FULL;
	} else {
		if (idx < v->total)
		{
			/* move elements*/
			for (i = idx; i < v->total - 1; i++)
			{
				v->items[i+1] = v->items[i];
			}
			v->items[idx] = Vector_New_Item(v->itemSize);
			if ( v->items[idx] != NULL) {
				Memcpy(v->items[idx],item, v->itemSize);
				v->total++;
				*ret = VECTOR_OK;
			} else {
				*ret = VECTOR_ERROR;
			}

		}else{
			*ret = VECTOR_INDEX;
		}
	}
}
vectorItem Vector_Back(vector const * const v, vectorErrorCode * const ret)
{
	return Vector_Get(v,Vector_End(v,ret)-1,ret);
}

void Vector_Set(vector * const v, iterator idx, vectorItem item, vectorErrorCode * const ret)
{
	if (idx < v->total) {
		Memcpy(v->items[idx],item, v->itemSize);
		*ret = VECTOR_OK;
	} else {
		*ret = VECTOR_INDEX;
	}
}

vectorItem Vector_Get(vector const * const v, iterator idx, vectorErrorCode * const ret)
{
	void *retItem;

	if (idx < v->total) {
		retItem = v->items[idx];
		*ret = VECTOR_OK;
	} else {
		retItem = NULL;
		*ret = VECTOR_INDEX;
	}
	return retItem;
}

void Vector_Delete(vector * const v, iterator idx, vectorErrorCode * const ret)
{
	iterator i;
	/* check if index is in range*/
	if (idx >= v->total) {
		*ret = VECTOR_INDEX;
	} else {
		Util_Free(CACHE_ENABLE,v->items[idx]);
		for (i = idx; i < v->total - 1; i++) {
			v->items[i] = v->items[i + 1];
		}
		if ( v->total > 0) {
			v->total--;
			*ret = VECTOR_OK;
		} else {
			v->total = 0;
			*ret = VECTOR_EMPTY;
		}
	}
}

void Vector_Swap(vector * const v, vector * const v2, vectorErrorCode * const ret)
{
	iterator i;
	vectorItem tempItem;

	if ( v->total == v2->total ) {
		for(i=0; i<v->total; i++) {
			tempItem = v->items[i];
			v->items[i] = v2->items[i];
			v2->items[i] = tempItem;
		}
		*ret = VECTOR_OK;
	} else {
		*ret = VECTOR_NO_SWAP;
	}
}

void Vector_Free(vector * const v, vectorErrorCode * const ret)
{
	iterator i;

	for (i = 0; i < v->total; i++) {
		Util_Free(CACHE_ENABLE,v->items[i]);
	}
	Util_Free(CACHE_ENABLE,v->items);
	v->total = 0;
	v->capacity = 0;
	*ret = VECTOR_OK;
}
