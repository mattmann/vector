/**
 * \file  vector.h
 *
 * \brief Interface for vector lib
 *
 * \version 1.0
 * \author Matthias Lehmann
 * \date 29.03.2017
 *
 * This file describes the exported functionality of the vector library.
 *
 *
 * @copyright
 *
 * Software History:
 * @date 29.03.2017 M.L. creation of file
 *
 *
*/

/** \addtogroup SCORPOS_LIBRARY SCORPOS_LIBRARY */

 /** \addtogroup VECTOR vector
  * \ingroup SCORPOS_LIBRARY
  * \brief  vector
  * \Description
  * Vector
Vectors are sequence containers representing arrays that can change in size.

Just like arrays, vectors use contiguous storage locations for their elements,
which means that their elements can also be accessed using offsets on regular
pointers to its elements, and just as efficiently as in arrays. But unlike
arrays, their size can change dynamically, with their storage being handled
automatically by the container.

Internally, vectors use a dynamically allocated array to store their elements.
This array may need to be reallocated in order to grow in size when new elements
are inserted, which implies allocating a new array and moving all elements to it.
This is a relatively expensive task in terms of processing time, and thus, vectors
do not reallocate each time an element is added to the container.

Instead, vector containers may allocate some extra storage to accommodate for
possible growth, and thus the container may have an actual capacity greater than
the storage strictly needed to contain its elements (i.e., its size).
Libraries can implement different strategies for growth to balance between memory
usage and reallocations, but in any case, reallocations should only happen at
logarithmically growing intervals of size so that the insertion of individual elements
at the end of the vector can be provided with amortized constant time complexity
(see push_back).

Therefore, compared to arrays, vectors consume more memory in exchange for the ability
to manage storage and grow dynamically in an efficient way.

Compared to the other dynamic sequence containers (deques, lists and forward_lists),
vectors are very efficient accessing its elements (just like arrays) and relatively
efficient adding or removing elements from its end. For operations that involve inserting
or removing elements at positions other than the end, they perform worse than the others,
and have less consistent iterators and references than lists and forward_lists.

*/
/*@{*/


#ifndef VECTOR_H
#define VECTOR_H

/* IMPORTED MODULES */

/* EXPORT CONSTANTS AND DATA TYPES */
/** \brief error code enumerator for error treatment */
typedef enum {
	VECTOR_OK     = 0,  /* Operation performed*/
	VECTOR_FULL   = 1,  /* no place vector is full*/
	VECTOR_EMPTY  = 2,  /* no element vector is empty*/
	VECTOR_INDEX  = 3,  /* vector index is not valid*/
	VECTOR_ERROR  = 4,  /* vector has trouble*/
	VECTOR_NO_SWAP= 5,  /* vector can not be swaped*/
}vectorErrorCode;

/** \brief iterator type definition to access vector by index */
typedef unsigned int iterator ;

/** \brief vector element data type */
typedef void *  vectorItem;

/** \brief initialize size of vector change value to the maximum vector needed*/
#define VECTOR_INIT_CAPACITY 4

/** \brief data structure of a vector*/
typedef struct  {
	vectorItem * items;    /**< item list for the vector*/
    unsigned int capacity; /**< vector maximum size */
    unsigned int total;    /**< vector actual size */
    unsigned int itemSize; /**< size of the item to be allocated in add and insert*/
} vector;

/* EXPORT DATA */

/* C extern variable declarations: */

/* LOCAL OPERATIONS */

/* EXPORT OPERATIONS */

/** \brief Initialize a Vector
 *
 * This function initializes the vector, and returns error code.
 * Initialize function must be called first before any other vector function.
 * \param v - (In) reference to a vector
 * \param itemSize - (In) size of the item in the vector
 * \param ret - (Out) Error code
 * \return None.
 * \retval VECTOR_OK - Successful completion
 */
void Vector_Init(vector * const v, unsigned int itemSize,  vectorErrorCode * const ret);

/** \brief Returns an iterator to the last element of the container.
 *
 * The function gives back the last iterator of the vector.
 * Initialize function must be called before use.
 * \param v - (In) reference of the vector
 * \param ret - (Out) Error code
 * \return None.
 * \retval VECTOR_OK - Successful completion
 */
iterator Vector_End(vector const * const v,vectorErrorCode * const ret);

/** \brief Adds an item to the end of the vector
 *
 * This function adds a new utem at the end of the list and returns error code.

 * \param v - (In) reference of the vector
 * \param ret - (Out) Error code
 * \return None.
 * \retval VECTOR_OK - Successful completion
 */
void Vector_Add(vector * const v, vectorItem item, vectorErrorCode * const ret);

/** \brief Insert an item at the given index (idx)
 *
 * The function moves all elements from index and insert a new item at index and returns error code.
 *
 * \param v - (In) reference of the vector
 * \param idx - (In) index of the vector
 * \param ret - (Out) Error code
 * \return None.
 * \retval VECTOR_OK - Successful completion
 */
void Vector_Insert(vector * const v, iterator idx, vectorItem item, vectorErrorCode * const ret);


/** \brief Sets the item of the vector at index
 *
 * The function set the item in the idx of the vector.
 * Initialize function must be called first.
 * \param v - (In) reference of the vector
 * \param idx - (In) index of the vector
 * \param item - (In) item placed into the vector
 * \param ret - (Out) Error code
 * \return None.
 * \retval VECTOR_OK - Successful completion
 */
void Vector_Set(vector * const v, iterator idx, vectorItem item, vectorErrorCode * const ret);

/** \brief Get the item from index of the vector
 *
 * This function returns the item of the idx of the vector.
 * Initialize function must be called first.
 * \param v - (In) reference of the vector
 * \param idx - (In) index of the vector
 * \param ret - (Out) Error code
 * \return None.
 * \retval VECTOR_OK - Successful completion
 * \retval VECTOR_INDEX - if the index is outside the range
 */
vectorItem Vector_Get(vector const * const v, iterator idx, vectorErrorCode * const ret);

/** \brief Deletes the item at index
 *
 * This function deletes the vector and moves the items.
 * Initialize function must be called first.
 * \param v - (In) reference of the vector
 * \param idx - (In) index of the vector
 * \param ret - (Out) Error code
 * \return None.
 * \retval VECTOR_OK - Successful completion
 */
void Vector_Delete(vector * const v, iterator idx, vectorErrorCode * const ret);

/** \brief Releases the memory of the vector
 *
 * This function frees the allocated memory of the vector.
 * 
 * \param v - (In) reference of the vector
 * \param ret - (Out) Error code
 * \return None.
 * \retval VECTOR_OK - Successful completion
 */
void Vector_Free(vector * const v, vectorErrorCode * const ret);

/** \brief Access last element
 *
 * This function returns the content of the last element.
 * 
 * \param v - (In) reference of the vector
 * \param ret - (Out) Error code
 * \return None.
 * \retval VECTOR_OK - Successful completion
* \retval VECTOR_INDEX - if the index is outside the range
 */
vectorItem Vector_Back( vector const * const v,vectorErrorCode * const ret);

/** \brief Swaps two vectors
 *
 * This function swaps two vectors form the same size.
 * 
 * \param v - (In) reference of the first vector
 * \param v2 - (In) reference of the second vector
 * \param ret - (Out) Error code
 * \return None.
 * \retval VECTOR_OK - Successful completion
 * \retval VECTOR_NO_SWAP - The vector v aund v2 have a different size
 */
void Vector_Swap(vector * const v, vector * const v2, vectorErrorCode * const ret);

/*@}*/
#endif
